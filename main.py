import sys
import numpy as np
from agents.agent import DDPG
from task import Task

num_episodes = 1000
init_pose = np.array([0, 0, 0.5, 0, 0, 0])
target_pos = np.array([0, 0, 10])

task = Task(init_pose, target_pos=target_pos)
agent = DDPG(task)
labels = ['time', 'x', 'y', 'z', 'phi', 'theta', 'psi', 'x_velocity',
          'y_velocity', 'z_velocity', 'phi_velocity', 'theta_velocity',
          'psi_velocity', 'rotor_speed1', 'rotor_speed2', 'rotor_speed3', 'rotor_speed4']

total_reward = []
for i_episode in range(1, num_episodes + 1):
    state = agent.reset_episode()
    res = {x: [] for x in labels}

    while True:
        action = agent.act(state)
        next_state, reward, done = task.step(action)
        agent.step(action, reward, next_state, done)
        state = next_state
        to_write = [task.sim.time] + list(task.sim.pose) + list(task.sim.v) + list(task.sim.angular_v) + list(action)

        for ii in range(len(labels)):
            res[labels[ii]].append(to_write[ii])

        if done:
            total_reward.append(agent.total_reward)
            print("\rEpisode = {:4d}, score = {:7.3f} (best = {:7.3f})".format(
                i_episode, agent.score, agent.best_score), end="")  # [debug]
            break
    sys.stdout.flush()
